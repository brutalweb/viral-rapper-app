
Krever:

* JDK (7 eller senere)
* Node.js
* Cordova (npm install -g cordova)
* Android SDK (http://developer.android.com/sdk/index.html)
** Trenger i utganspunktet ikke Android Studio
* Start sdk/tools/android og installer
** Android 5.1.1 (API 22) platform SDK
** Android SDK Build-tools version 19.1.0 (eller senere)
** Android Support Repository (Extras)

Når dette er installert skal du kunne kjøre (fra denne katalogen):

cordova build

Og så for å kjøre:

cordova run --emulator

Med telefon koblet i:

cordova run --device

