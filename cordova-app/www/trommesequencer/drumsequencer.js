	var log = console.log.bind(console);
	var ctx = new(window.AudioContext || window.webkitAudioContext)();
	var mainGain = ctx.createGain();
	var recorderGain = ctx.createGain();
	mainGain.connect(ctx.destination);
	var drumboxModule = angular.module('drumboxModule', []);

	drumboxModule.controller('DrumboxController', ['$scope', '$http', function($scope, $http) {
		$scope.sounds = [];
		$scope.bpm = 75;
		$scope.frames = 16;
		$scope.playing = false;
		$scope.startBox = function() {
			if(!$scope.playing) {
				$scope.playing = true;
				$scope.stepBox(0);
			}

		};
		$scope.stopBox = function() {
			$scope.playing = false;
		}
		$scope.stepBox = function(frame) {
			$scope.sounds.forEach(function(sound) {
				if(sound.slots[frame].value) {
					sound.play();
				}
			});
			
			if($scope.playing) {
				var nextFrame = frame+1;
				if(nextFrame >= $scope.frames) {
					nextFrame = 0;
				}
				log(nextFrame);
				//TODO requestAnimFrame?
				setTimeout(function() {$scope.stepBox(nextFrame)}, 60000/$scope.bpm/4);
			}
		};


		var soundFileNames = ['cl_hihat', 'claves', 'conga1', 'cowbell', 'crashcym', 'handclap', 'hi_conga', 'hightom', 'kick1', 'kick2', 'maracas', 'open_hh', 'rimshot', 'snare', 'tom1'];
		soundFileNames.forEach(function(filename) {
			var url = 'trommesequencer/drumkit/' + filename + '.mp3';
			$http.get(url, {responseType : 'arraybuffer'}).success(function(data, status, headers, config) {
				ctx.decodeAudioData(data, function(buffer) {
					var soundObject = {
						name : filename,
						sound: buffer,
						play : function() {
							var source = ctx.createBufferSource();
							source.buffer = this.sound;
							source.connect(mainGain);
							source.connect(recorderGain);
							source.start(0);
						}
					}
					soundObject.slots = [];
					for (var i = 1; i <= $scope.frames; i++) {
						var slot = {
							number : i,
							value : false
						}
						soundObject.slots.push(slot);
					};
					$scope.sounds.push(soundObject);
					$scope.$apply();


					//Let's make a little beat ready for them and start it
					$scope.sounds[0].slots[0].value = true;
					$scope.sounds[0].slots[3].value = true;

					$scope.sounds[1].slots[1].value = true;

					$scope.sounds[4].slots[4].value = true;

					$scope.sounds[5].slots[12].value = true;

					$scope.sounds[6].slots[9].value = true;
					$scope.sounds[6].slots[11].value = true;

					$scope.sounds[7].slots[10].value = true;

					$scope.sounds[8].slots[0].value = true;
					$scope.sounds[8].slots[6].value = true;
					$scope.sounds[8].slots[7].value = true;

					$scope.sounds[10].slots[12].value = true;
					$scope.sounds[10].slots[13].value = true;
					$scope.sounds[10].slots[14].value = true;
					$scope.sounds[10].slots[15].value = true;

					$scope.sounds[12].slots[2].value = true;

					//let's even start it without permission!
					//$scope.startBox();
					//or not
				}, function(err) {
					log(err);
				});
			}).error(function(data, status, headers, config) {
				log("error status " + status);
			});
		});
	}]);